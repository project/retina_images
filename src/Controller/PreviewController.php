<?php

namespace Drupal\retina_images\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\image\ImageStyleInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PreviewController. Preview image style.
 *
 * @package Drupal\retina_images\Controller
 */
class PreviewController extends ControllerBase {

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Image settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * A date time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a PreviewController object.
   *
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   Provides a factory for image objects.
   * @param \Psr\Log\LoggerInterface $logger
   *   Describes a logger instance.
   * @param \Drupal\Core\Config\ImmutableConfig $immutable_config
   *   Defines the immutable configuration object.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   Generates file URLs for a stream to an external or local file.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Defines an interface for obtaining system time.
   */
  public function __construct(ImageFactory $image_factory, LoggerInterface $logger, ImmutableConfig $immutable_config, FileUrlGeneratorInterface $file_url_generator, TimeInterface $time) {
    $this->imageFactory = $image_factory;
    $this->logger = $logger;
    $this->config = $immutable_config;
    $this->fileUrlGenerator = $file_url_generator;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('image.factory'),
      $container->get('logger.factory')->get('image'),
      $container->get('config.factory')->get('image.settings'),
      $container->get('file_url_generator'),
      $container->get('datetime.time')
    );
  }

  /**
   * Deliver a retina version of the preview image.
   */
  public function preview(Request $request, ImageStyleInterface $image_style) {
    $original_path = $this->config->get('preview_image');

    // Set up derivative file information.
    $preview_file = $image_style->buildUri($original_path);

    // Create derivative if necessary.
    if (!file_exists($preview_file)) {
      $image_style->createDerivative($original_path, $preview_file);
    }

    $preview_image = $this->imageFactory->get($preview_file);
    $variables['derivative'] = [
      'url' => $this->fileUrlGenerator->generateString($preview_file),
      'width' => $preview_image->getWidth(),
      'height' => $preview_image->getHeight(),
    ];
    $image_style->transformDimensions($variables['derivative'], $preview_file);

    return [
      '#theme' => 'image',
      '#uri' => $variables['derivative']['url'] . '?cache_bypass=' . $this->time->getRequestTime(),
      '#attributes' => [
        'width' => $variables['derivative']['width'],
        'height' => $variables['derivative']['height'],
      ],
      '#suffix' => '<br />' . $image_style->toLink($this->t('Back'), 'edit-form')->toString(),
    ];
  }

}
