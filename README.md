# Retina Images

Retina Images adds an option to all image effects included with core to allow them
to output high resolution images for high DPI or retina displays. When enabled as part of an image style,
this module returns high resolution images for all devices.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/retina_images).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/retina_images).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Enable the 'Retinafy' checkbox for the image style effect.


## Maintainers

- Michael Prasuhn - [mikey_p](https://www.drupal.org/u/mikey_p)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
- Oleh Vehera - [voleger](https://www.drupal.org/u/voleger)
